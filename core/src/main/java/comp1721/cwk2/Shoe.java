package comp1721.cwk2;
import java.util.Collections;

public class Shoe extends CardCollection
{

  /**
   * Stores the specified number of complete decks of cards into the shoe.
   *
   * @param decks Number of decks of cards the game should have.
   */
  public Shoe(int decks)
  {
    if (decks != 6 && decks != 8)
    {
      throw new CardException("Invalid value for decks :" + decks
          + "\nShould be 6 or 8");
    }
    for (int i = 0; i < decks; i++)
    {
      createDeck();
    }
  }

  /**
   * Reorders the cards in the shoe randomly.
   */
  public void shuffle()
  {
    Collections.shuffle(cards);
  }

  /**
   * Provides the first card in the shoe and removes it from the shoe.
   *
   * @return The first stored card in the shoe.
   */
  public Card deal()
  {
    if (cards.size() == 0)
    {
      throw new CardException("Could not deal card, shoe is empty");
    }
    else
    {
      Card thisCard = cards.get(0);
      cards.remove(0);
      return thisCard;
    }
  }

  /**
   * Create a deck of 52 cards ordered first by suit, then rank.
   */
  private void createDeck()
  {
    for (Card.Suit suit : Card.Suit.values())
    {
      for (Card.Rank rank: Card.Rank.values())
      {
        BaccaratCard card = new BaccaratCard(rank, suit);
        add(card);
      }
    }
  }
}