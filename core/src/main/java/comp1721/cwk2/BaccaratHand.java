package comp1721.cwk2;

public class BaccaratHand extends CardCollection
{
  /**
   * Creates a two-character-representation of each card in this hand,
   * each separated with a space.
   *
   * @return String of the hand.
   */
  public String toString()
  {
    String hand = "";
    for (int i = 0; i < cards.size(); i++)
    {
      hand += cards.get(i);
      if (i != cards.size() - 1)
      {
        hand = hand + " ";
      }
    }
    return hand;
  }


  /**
   * Iterates through the hand and totals their value to see if the hand
   * contains a natural.
   * If the value is equal to 8 or 9 the hand contains a natural.
   *
   * @return True if hand contains a natural, false otherwise.
   */
  public boolean isNatural()
  {
    int total = 0;
    for (Card card : cards) {
      total += card.value();
    }
    return total == 8 || total == 9;
  }


  /**
   * Iterates through this hand and calculates its total value.
   * If the value exceeds 9, only the unit value is retained,
   * e.g. 13 becomes 3 and 17 becomes 7.
   *
   * @return Value of this hand.
   */
  @Override
  public int value() {
    int total = 0;
    for (Card card: cards) {
      total += card.value();
    }
    if (total > 9)
    {
      total %= 10;
    }

    return total;
  }
}