package comp1721.cwk2;

public class BaccaratCard extends Card
{
  /**
   * Creates a BaccaratCard object.
   *
   * @param r Rank of the card.
   * @param s Suit of the card.
   */
  public BaccaratCard(Rank r, Suit s)
  {
    super(r,s);
  }

  /**
   * Calculates the value of this card.
   * Value is based on the card's rank.
   * Any card with a value above 9 is changed to 0.
   *
   * @return Value of the card .
   */
  @Override
  public int value()
  {
    int cardValue = Math.min(getRank().ordinal() + 1, 10);
    if (cardValue > 9)
    {
      cardValue -= 10;
    }
    return cardValue;
  }
}