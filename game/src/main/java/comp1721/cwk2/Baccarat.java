package comp1721.cwk2;

import java.util.Random;
import java.util.Scanner;

public class Baccarat {
  private static Shoe game;
  private static BaccaratHand player;
  private static BaccaratHand banker;
  private static int numRounds;
  private static int playerWins;
  private static int bankerWins;
  private static int ties;

  /**
   * Checks whether the player or banker wins the round or if there was a tie.
   */
  private static void checkWinner()
  {
    if (player.value() == banker.value())
    {
      ties++;
      System.out.println("Round tie.\n");
    }
    else if (player.value() > banker.value())
    {

      playerWins++;
      System.out.println("Player win!\n");
    }
    else
    {
      bankerWins++;
      System.out.println("Banker win!\n");
    }
  }


  /**
   * Discards the hands of both the player and the banker, ready for
   * another round.
   */
  private static void clearHands()
  {
    player.discard();
    banker.discard();
  }


  /**
   * Decides whether the player or the banker should draw a third card.
   */
  private static void drawThirdCard()
  {
    boolean dealBanker = false;
    //player only draws another card if their hand value is 0-5
    if (player.value() >= 0 && player.value() <= 5)
    {
      System.out.println("Dealing third card to player...");
      player.add(game.deal());

      //then the banker acts according to additional rules
      if (banker.value() <= 2)
      {
        dealBanker = true;
      }
      else
      {
        //banker draws depending on the value of players' 3rd card
        int p3rdValue = player.cards.get(2).value();

        switch (banker.value())
        {
          case 3:
            if (p3rdValue != 8)
            {
              dealBanker = true;
            }
            break;

          case 4:
            if (p3rdValue >= 2 && p3rdValue <= 7)
            {
              dealBanker = true;
            }
            break;

          case 5:
            if (p3rdValue >= 4 && p3rdValue <= 7)
            {
              dealBanker = true;
            }
            break;

          case 6:
            if (p3rdValue == 6 || p3rdValue == 7)
            {
              dealBanker = true;
            }
            break;
        }
      }
    }
    //otherwise they stand and the banker only draws a third card if their hand
    //value is 0-5
    else if (banker.value() >= 0 && banker.value() <= 5)
    {
      dealBanker = true;
    }
    if (dealBanker)
    {
      System.out.println("Dealing third card to banker...");
      banker.add(game.deal());
    }

    System.out.println("Player: " + player.toString() + " = " + player.value());
    System.out.println("Banker: " + banker.toString() + " = " + banker.value());
    checkWinner();
  }


  /**
   * Deals a hand of two cards to both the player and the banker.
   *
   * If either hand holds a natural, no more cards are drawn and the higher
   * value hand wins.
   *
   * If there is no natural, a third card may be drawn from either hand,
   * depending on the game rules.
   */
  private static void playRound()
  {
    for (int i = 0; i < 2; i++)
    {
      player.add(game.deal());
      banker.add(game.deal());
    }
    System.out.println("Player: " + player.toString() + " = " + player.value());
    System.out.println("Banker: " + banker.toString() + " = " + banker.value());

    if (player.isNatural() || banker.isNatural())
    {
      checkWinner();
    }
    else
    {
      drawThirdCard();
    }
    numRounds++;
    clearHands();
  }


  /**
   * Asks the user if they wish to play another round of the game
   *
   * @return True if the user wants another round, false otherwise
   */
  private static boolean askAnotherRound()
  {
    boolean anotherRound;
    Scanner scan = new Scanner(System.in);
    String input;
    //continue asking until correct input is received
    while(true)
    {
      System.out.print("Another round? (y/n): ");
      input = scan.next();
      if (input.equals("y"))
      {
        anotherRound = true;
        break;
      }
      else if (input.equals("n"))
      {
        anotherRound = false;
        break;
      }
    }
    return anotherRound;
  }

  /**
   * Prints the results of the game to the console.
   * Includes the number of rounds played, number of player wins,
   * number of banker wins and number of ties
   */
  private static void displayResults()
  {
    System.out.println(numRounds + " rounds played");
    System.out.println(playerWins + " player wins");
    System.out.println(bankerWins + " banker wins");
    System.out.println(ties + " ties");
  }

  /**
   * Creates a Baccarat game using 6 or 8 decks shuffled together
   * and initialises game variables
   */
  private static void setUpGame()
  {
    //use ternary condition on random to decide number of decks to use
    Random random = new Random();
    int decks = random.nextBoolean()? 6:8;
    game = new Shoe(decks);
    game.shuffle();
    player = new BaccaratHand();
    banker = new BaccaratHand();
    numRounds = 0;
    playerWins = 0;
    bankerWins = 0;
    ties = 0;
  }

  public static void main(String[] args) {
    boolean interactive = false;
    if (args.length != 0)
    {
      if (args[0].equals("-i"))
      {
        interactive = true;
      }
    }

    setUpGame();

    //play rounds until there are not enough cards to play,
    //or if user wants to stop (interactive mode only)
    while (game.size() >= 6 )
    {
      playRound();
      if (game.size() >= 6 && interactive)
      {
        if (!askAnotherRound())
        {
          break;
        }
      }
    }
    displayResults();
  }
}
